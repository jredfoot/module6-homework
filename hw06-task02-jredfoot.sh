#!/usr/bin/bash
baseURL="https://www.bennetyee.org/ucsd-pages/area.html"
read -p "Enter an area code: " area_code

if [ -z "$area_code" ] ; then
echo "usage: areacode <three-digit US telephone area code>"
exit 1
fi

if [ "$(echo $area_code | wc -c)" -ne 4 ] ; then
echo "areacode: wrong length: only works with three-digit US area codes"
exit 1
fi

if [ ! -z "$(echo $area_code | sed 's/[[:digit:]]//g')" ]; then
echo "areacode: not-digits: area codes can only be made up of digits"
exit 1
fi

city="$(curl -s -dump $baseURL | grep "name=\"$area_code" | sed 's/<[^>]*>//g;s/^ //g' | cut -f2- -d\ | cut -f1 -d\( )"

echo "Area code $area_code =$city"
