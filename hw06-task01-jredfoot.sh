#!/bin/usr/bash
baseURL="http://www.city-data.com/zips"
read -p "Enter a zip code: " zipcode

city=$(curl -s -dump "$baseURL/$zipcode.html" | grep -i '<title>' | cut -d\( -f2 | cut -d\) -f1)

echo -n "Zip code $zipcode is in $city" 
echo " "
